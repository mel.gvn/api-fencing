<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190513113306 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE weapon_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE weapon_type_fencer (weapon_type_id INT NOT NULL, fencer_id INT NOT NULL, INDEX IDX_78D411A3607BCCD7 (weapon_type_id), INDEX IDX_78D411A3D76841B (fencer_id), PRIMARY KEY(weapon_type_id, fencer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fencer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, photo VARCHAR(255) DEFAULT NULL, age INT NOT NULL, handness VARCHAR(255) NOT NULL, start INT NOT NULL, end INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE weapon_type_fencer ADD CONSTRAINT FK_78D411A3607BCCD7 FOREIGN KEY (weapon_type_id) REFERENCES weapon_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE weapon_type_fencer ADD CONSTRAINT FK_78D411A3D76841B FOREIGN KEY (fencer_id) REFERENCES fencer (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE weapon_type_fencer DROP FOREIGN KEY FK_78D411A3607BCCD7');
        $this->addSql('ALTER TABLE weapon_type_fencer DROP FOREIGN KEY FK_78D411A3D76841B');
        $this->addSql('DROP TABLE weapon_type');
        $this->addSql('DROP TABLE weapon_type_fencer');
        $this->addSql('DROP TABLE fencer');
    }
}
