<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190513200356 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fencer_season (fencer_id INT NOT NULL, season_id INT NOT NULL, INDEX IDX_45DB6B06D76841B (fencer_id), INDEX IDX_45DB6B064EC001D1 (season_id), PRIMARY KEY(fencer_id, season_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fencer_season ADD CONSTRAINT FK_45DB6B06D76841B FOREIGN KEY (fencer_id) REFERENCES fencer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fencer_season ADD CONSTRAINT FK_45DB6B064EC001D1 FOREIGN KEY (season_id) REFERENCES season (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE result');
        $this->addSql('DROP TABLE season_fencer');
        $this->addSql('ALTER TABLE season CHANGE years years VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE result (id INT AUTO_INCREMENT NOT NULL, fencer_id INT DEFAULT NULL, season_id INT DEFAULT NULL, points INT DEFAULT NULL, INDEX IDX_136AC1134EC001D1 (season_id), INDEX IDX_136AC113D76841B (fencer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE season_fencer (season_id INT NOT NULL, fencer_id INT NOT NULL, INDEX IDX_1F2F31D04EC001D1 (season_id), INDEX IDX_1F2F31D0D76841B (fencer_id), PRIMARY KEY(season_id, fencer_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC1134EC001D1 FOREIGN KEY (season_id) REFERENCES season (id)');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC113D76841B FOREIGN KEY (fencer_id) REFERENCES fencer (id)');
        $this->addSql('ALTER TABLE season_fencer ADD CONSTRAINT FK_1F2F31D04EC001D1 FOREIGN KEY (season_id) REFERENCES season (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE season_fencer ADD CONSTRAINT FK_1F2F31D0D76841B FOREIGN KEY (fencer_id) REFERENCES fencer (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE fencer_season');
        $this->addSql('ALTER TABLE season CHANGE years years VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
