<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $start;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $end;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Fencer", inversedBy="events")
     */
    private $fencers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Season", inversedBy="events")
     */
    private $season;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\WeaponType", inversedBy="events")
     */
    private $weaponTypes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Team", inversedBy="events")
     */
    private $teams;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $points;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rank;

    public function __construct()
    {
        $this->fencers = new ArrayCollection();
        $this->season = new ArrayCollection();
        $this->weaponTypes = new ArrayCollection();
        $this->teams = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(?\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(?\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    /**
     * @return Collection|Fencer[]
     */
    public function getFencers(): Collection
    {
        return $this->fencers;
    }

    public function addFencer(Fencer $fencer): self
    {
        if (!$this->fencers->contains($fencer)) {
            $this->fencers[] = $fencer;
        }

        return $this;
    }

    public function removeFencer(Fencer $fencer): self
    {
        if ($this->fencers->contains($fencer)) {
            $this->fencers->removeElement($fencer);
        }

        return $this;
    }

    /**
     * @return Collection|Season[]
     */
    public function getSeason(): Collection
    {
        return $this->season;
    }

    public function addSeason(Season $season): self
    {
        if (!$this->season->contains($season)) {
            $this->season[] = $season;
        }

        return $this;
    }

    public function removeSeason(Season $season): self
    {
        if ($this->season->contains($season)) {
            $this->season->removeElement($season);
        }

        return $this;
    }

    /**
     * @return Collection|WeaponType[]
     */
    public function getWeaponTypes(): Collection
    {
        return $this->weaponTypes;
    }

    public function addWeaponType(WeaponType $weaponType): self
    {
        if (!$this->weaponTypes->contains($weaponType)) {
            $this->weaponTypes[] = $weaponType;
        }

        return $this;
    }

    public function removeWeaponType(WeaponType $weaponType): self
    {
        if ($this->weaponTypes->contains($weaponType)) {
            $this->weaponTypes->removeElement($weaponType);
        }

        return $this;
    }

    /**
     * @return Collection|Team[]
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Team $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
        }

        return $this;
    }

    public function removeTeam(Team $team): self
    {
        if ($this->teams->contains($team)) {
            $this->teams->removeElement($team);
        }

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(?int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }
}
