<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SeasonRepository")
 */
class Season
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $years;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $total;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Fencer", mappedBy="seasons")
     */
    private $fencers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Event", mappedBy="season")
     */
    private $events;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Team", inversedBy="seasons")
     */
    private $teams;

    public function __construct()
    {
        $this->fencers = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->teams = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getYears(): ?string
    {
        return $this->years;
    }

    public function setYears(?string $years): self
    {
        $this->years = $years;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(?int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return Collection|Fencer[]
     */
    public function getFencers(): Collection
    {
        return $this->fencers;
    }

    public function addFencer(Fencer $fencer): self
    {
        if (!$this->fencers->contains($fencer)) {
            $this->fencers[] = $fencer;
            $fencer->addSeason($this);
        }

        return $this;
    }

    public function removeFencer(Fencer $fencer): self
    {
        if ($this->fencers->contains($fencer)) {
            $this->fencers->removeElement($fencer);
            $fencer->removeSeason($this);
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->addSeason($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            $event->removeSeason($this);
        }

        return $this;
    }

    /**
     * @return Collection|Team[]
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Team $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
        }

        return $this;
    }

    public function removeTeam(Team $team): self
    {
        if ($this->teams->contains($team)) {
            $this->teams->removeElement($team);
        }

        return $this;
    }
}
