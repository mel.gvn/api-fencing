<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;
use App\Repository\FencerRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Fencer;

/**
 * @Route("api-fencing/fencers/")
 */
class FencerController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods="GET")
     */
    public function getAllFencers(FencerRepository $fencerRepo)
    {
        $fencers = $fencerRepo->findAll();
        $json = $this->serializer->serialize($fencers, 'json');
        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("{fencer}", methods="GET")
     */
    public function getOneFencer(Fencer $fencer)
    {
        return new JsonResponse($this->serializer->serialize($fencer, 'json'), 200, [], true);
    }
}
