<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;
use App\Repository\SeasonRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Season;

/**
 * @Route("api-fencing/seasons")
 */
class SeasonController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    public function __construct(SerializerInterface $serializerInterface)
    {
       $this->serializer = $serializerInterface;
    }

    /**
     * @Route(methods="GET")
     */
    public function getAllSeasons(SeasonRepository $seasonRepo)
    {
        $season = $seasonRepo->findAll();
        $json = $this->serializer->serialize($season, 'json');
        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/{season}", methods="GET")
     */
    public function getOneSeason(Season $season)
    {
        return new JsonResponse($this->serializer->serialize($season, 'json'), 200, [], true);
    }
}
