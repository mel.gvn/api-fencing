<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;
use App\Repository\TeamRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Team;

/**
 * @Route("api-fencing/teams")
 */
class TeamController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods="GET")
     */
    public function getAllTeams(TeamRepository $teamRepo)
    {
        $team = $teamRepo->findAll();
        $json = $this->serializer->serialize($team, 'json');
        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("{team}", methods="GET")
     */
    public function getOneTeam(Team $team)
    {
        return new JsonResponse($this->serializer->serialize($team, 'json'), 200, [], true);
    }
}
