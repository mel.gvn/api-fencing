<?php

namespace App\Repository;

use App\Entity\WeaponTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WeaponTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeaponTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeaponTypes[]    findAll()
 * @method WeaponTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeaponTypesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WeaponTypes::class);
    }

    // /**
    //  * @return WeaponTypes[] Returns an array of WeaponTypes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WeaponTypes
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
