<?php

namespace App\services;

use Symfony\Component\HttpFoundation\File\File;


class UploadService {

    public function upload(File $image):string {
        $name = md5( uniqid() ) . '.' . $image->guessExtension();
        $image->move($_ENV['UPLOAD_DIRECTORY'], $name);
        return $name;
    }
}